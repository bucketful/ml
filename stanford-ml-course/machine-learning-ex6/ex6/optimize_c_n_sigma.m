fprintf('Starting program.\n');

load('ex6data3.mat');


% Try different SVM Parameters here
% [C, sigma] = dataset3Params(X, y, Xval, yval);

multArray = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];


C = multArray(5);
for j = 3:3
	sigma = multArray(j);

	% Train the SVM
	model= svmTrain(X, y, C, @(x1, x2) gaussianKernel(x1, x2, sigma));
	% visualizeBoundary(X, y, model);

	subplot(2, 4, j);
	hold on
	plotData(Xval, yval);
	
	visualizeBoundary(Xval, yval, model);
	hold off
end



