function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

n = length(theta);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %
    err_vector = X * theta - y;
    theta_change = X' * err_vector;
    theta = theta - alpha * theta_change / m;

    % theta = theta - alpha * X' * (X * theta - y) / m;

    % oldtheta = zeros(n, 1);
    % for i = 1: n
    %     oldtheta(i) = theta(i);
    % end

    % for j = 1: n
    %     h = 0;
    %     for i = i: m
    %         h = h + (X(i, :) * oldtheta - y(i)) * X(i, j);
    %     end

    %     theta(j) = oldtheta(j) - (alpha / m) * h;
    % end

    % delta = zeros(m, 1);
    % for i = 1: m
    %     delta(i) = (X(i, :) * theta - y(i)) .* X(:, 2);
    % end
    % theta = theta - alpha * delta / m;

    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);
    % fprintf('%0.4f  %0.4f \n', theta(1), theta(2));
end

% J_history

% fprintf('\n J = %0.4f, theta(0) = %0.4f, theta(1) = %0.4f \n', J_history(length(J_history)), theta(1), theta(2));

end
