function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.


J = sum( (X * theta - y) .^ 2 ) / (2 * m);

% J = (X * theta - y)' * (X * theta - y) / (2 * m);

% for i = 1: m
%     % hx = 0;
%     % for j = 1: length(theta)
%     %     hx = hx + theta(j) * X(i, j);
%     % end

%     hx = X(i, :) * theta;
%     J = J + ((hx - y(i)).^ 2 / (2 * m));
% end

%     % h = X * theta;
%     % J = 1/(2*m) * sum(h - y) .^ 2;
% hx = X * theta;

% =========================================================================

end


