%reload_ext autoreload
%autoreload 2
import machine.client as cl
path = cl.get_path('digits')

test = cl.load_numpy_data(cl.get_path('digits') + 'data', 'kaggle_digits_mnist_test')

X = cl.load_numpy_data(cl.get_path('digits') + 'data', 'kaggle_digits_mnist_X')

Y = cl.load_numpy_data(cl.get_path('digits') + 'data', 'kaggle_digits_mnist_Y')

