from sklearn import svm

class SVM(object):
	
	def __init__(self, kernel=None, gamma=0.001):
		if kernel is None:
			self.classifier = svm.SVC(gamma)
		else:
			self.classifier = svm.SVC(kernel=kernel, gamma=gamma)

	def train(self, train_data, train_label):
		self.classifier.fit(train_data, train_label)
		return

	def predict(self, test_data):
		return self.classifier.predict(test_data)