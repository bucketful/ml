
# from numpy import concatenate, apply_along_axis, reshape, zeros, random, rotate, vstack, hstack
import numpy as np
from scipy.ndimage import convolve
import scipy.ndimage as nd

IMAGE_WIDTH = 28

class IMAGE(object):

    # def normalize(self, data):
    #   for i in range(len(data)):
    #       data[i] = data[i] / 255.0
    #   return data

    @classmethod
    def nudge_dataset(cls, X, Y):
        nudge_size = 2
        direction_vectors = [
            [[0, 1, 0],
             [0, 0, 0],
             [0, 0, 0]],

            [[0, 0, 0],
             [1, 0, 0],
             [0, 0, 0]],

            [[0, 0, 0],
             [0, 0, 1],
             [0, 0, 0]],

            [[0, 0, 0],
             [0, 0, 0],
             [0, 1, 0]]]

        shift = lambda x, w: convolve(x.reshape((IMAGE_WIDTH, IMAGE_WIDTH)), 
                            mode='constant',
                            weights=w).ravel()
        X = np.concatenate([X] + [np.apply_along_axis(shift, 1, X, vector)
                                for vector in direction_vectors])
        Y = np.concatenate([Y for _ in range(5)], axis=0)
        return X, Y

        # scaled_direction_matricies = [[[comp*nudge_size for comp in vect] for vect in matrix] for matrix in direction_vectors]
        
        # shift = lambda x, w: convolve(x.reshape((IMAGE_WIDTH, IMAGE_WIDTH)), mode='constant',
        #                               weights=w).ravel()
        # X = np.concatenate([X] +
        #                    [np.apply_along_axis(shift, 1, X, vector)
        #                     for vector in scaled_direction_matricies])

        # Y = np.concatenate([Y for _ in range(5)], axis=0)
        # return X, Y

    @classmethod
    def rotate_dataset(cls, X,Y):
        XX = np.zeros(X.shape)
        for index in range(X.shape[0]):
            angle = np.random.randint(-7,7)
            XX[index,:] = nd.rotate(np.reshape(X[index,:],((28,28))),angle,reshape=False).ravel()
        X = np.vstack((X,XX))
        Y = np.hstack((Y,Y))
        return X, Y

        