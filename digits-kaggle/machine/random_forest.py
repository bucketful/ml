from sklearn.ensemble import RandomForestClassifier

class Random_Forest(object):
	
	def __init__(self, n_estimators=100):
		self.classifier = RandomForestClassifier(n_estimators)

	def train(self, train_data, train_label):
		self.classifier.fit(train_data, train_label)
		return

	def predict(self, test_data):
		return self.classifier.predict(test_data)
