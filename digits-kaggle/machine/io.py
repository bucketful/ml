import os
from numpy import genfromtxt, savetxt, random, arange
import pickle

class IO(object):

	def __init__(self):
		pass

	@classmethod
	def get_path(cls, project):
		curr_dir = os.getcwd() + '/data/'

		if project == 'digits':
			return curr_dir + 'digits/'
		else:
			return None

	def read(self, 
			 file, 
			 data_start_col,
			 data_end_col,
			 label=True,
			 label_col=0,
			 header=True, 
			 include_header=True,
			 delimiter=',', 
			 dtype='f8'):

		label_data = None
		# if header/column_label exists, include it?
		if header and not include_header:
			first_row = 1
		else:
			first_row = 0

		# open file to read in data
		dataset = genfromtxt(open(file,'r'), 
			                 delimiter=delimiter, 
			                 dtype='f8')[first_row:] 
		# get labels from label column
		if label:
			label_data = [x[label_col] for x in dataset]

		# get data from data columns
		if data_end_col < 0:
			data = [x[data_start_col:] for x in dataset]
		else:
			data = [x[data_start_col:data_end_col] for x in dataset]
		return data, label_data

	@classmethod
	def save_numpy_data(cls, data, file_path, file_name=None):
		if file_name is None:
			fullfilename = file_path
		else:
			fullfilename = file_path + '/' + file_name

		try:
			with open(fullfilename, 'wb') as pfile:
				pickle.dump(data, pfile, protocol=pickle.HIGHEST_PROTOCOL)
		except Exception, e:
			print "exception saving %s" % fullfilename
		return

	@classmethod
	def get_numpy_data(cls, file_path, file_name=None):
		if file_name is None:
			fullfilename = file_path
		else:
			fullfilename = file_path + '/' + file_name

		try:
			with open(fullfilename, 'rb') as pfile:
				data = pickle.load(pfile)
		except Exception, e:
			print "exception loading %s" % fullfilename
		else:
			return data

	def get_shuffle_index(self, array_length):
		# array or size must be declared
		if array_length <= 0:
			return None

		# if necessary, create a sequence to be used as shuffle index
		arr = [i for i in range(array_length)]

		# randomly shuffle the array
		random.shuffle(arr)
		return arr

	def shuffle_array(self, arr, shuffled_index):
		# shuffle index and array to be shuffled must be same length
		length = len(shuffled_index)
		if len(arr) != length:
			return None

		# if necessary, create a sequence to be used as shuffle index
		shuffled_arr = [0 for i in range(length)]

		# copy over one element at a time based on shuffled_index
		for i in range(length):
			shuffled_arr[i] = arr[shuffled_index[i]]
		return shuffled_arr	

	def shuffle_matrix_rows(self, matrix, shuffled_index):
		rows = len(matrix)
		cols = len(matrix[0])

		# initialize a matrix to be returned
		shuffled = [[0 for i in range(cols)] for j in range(rows)]
		# copy over one row at a time based on shuffled_index
		for i in range(rows):
			shuffled[i] = matrix[shuffled_index[i]]
		return shuffled			

	def get_submission(self, 
						fullfilename, 
						X, Y, 
						fmt,
						header, 
						delimiter):
		# create numpy array[[X][Y]]
		arr = [[0 for i in range(2)] for i in range(len(Y))]
		for i in range(len(Y)):
				arr[i] = [X[i],Y[i]]
		try:
			with open(fullfilename, 'wb') as f:
				f.write(header + b'\n')
				#f.write(bytes("SP,"+lists+"\n","UTF-8"))
				#Used this line for a variable list of numbers
				savetxt(f, arr, fmt=fmt, delimiter=delimiter)
		except Exception, e:
			print "exception saving %s" % fullfilename
		else:
			print "%s saved" % fullfilename
			return 

	def save_results(self):
		pass

	def get_score(self, X, Y):
		length = len(X)
		if length != len(Y):
			return None

		correct = 0.0
		for i in range(length):
			if abs(X[i] - Y[i]) < (1**-10):
				correct += 1.0
		return correct / length


