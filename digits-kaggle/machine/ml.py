from machine.io import IO


class ML(object):

    def __init__(self):
		pass

	def read_input_file(self, 
						 file, 
						 data_start_col,
						 data_end_col,
						 label_col=True,
						 label_col_col=0,
						 header=True, 
						 include_header=True,
						 delimiter=',', 
						 dtype='f8'):
		io = IO();
		label, data = io.read(file, 
							 data_start_col,
							 data_end_col,
							 True,
							 0,
							 True, 
							 False)


	def split(self, data, ratio):
		pass

	def fit(self, method, train):
		pass

	def predict(self, trained_classifier, test_data)
		pass
		