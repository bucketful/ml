from nolearn.dbn import DBN

class DBN(object):

    def __init__(self, n_features, hidden_nodes, output_nodes, epochs,
    	         learn_rates = 3, learn_rate_decay = 0.9, verbose = 1):
        # self.n_features = n_features
        # self.hidden_nodes = hidden_nodes
        # self.output_nodes = output_nodes
        # self.learn_rates = learn_rates
        # self.learn_rate_decay = learn_rate_decay
        # self.epochs = epochs
        # self.verbose = verbose
        self.classifier = DBN([n_features, hidden_nodes, output_nodes], 
        	                   epochs=epochs, learn_rates=learn_rates, 
        	                   learn_rate_decays=learn_rate_decays, 
        	                   verbose=verbose)

    def train(self, train_data, train_label):
        self.classifier.fit(train_data, train_label)
        return

    def predict(self, test_data):
        return self.classifier.predict(test_data)