
from machine.io import IO
# from machine.svm import SVM
from machine.random_forest import Random_Forest
from time import time 

def test():
	io = IO()
	io.test()
	return

def read_input(filepath, filename, data_start_col, data_end_col,
	           label=True, label_col=0, header=True, include_header=True,
	           delimiter=',', dtype = 'f8'):
	io = IO()
	fullfilename = filepath + '/' + filename
	data, label_data = io.read(fullfilename, data_start_col, data_end_col, 
		label, label_col, header, include_header)
	print "input file %s read" %filename
	if label:
		return data, label_data
	else:
		return data

# save train and label data as numpy arrays
def save_numpy_train(filepath, train, target, train_name='train', target_name='target'):
	io = IO()
	io.save_numpy_data(train, filepath, train_name)
	io.save_numpy_data(target, filepath, target_name)
	print "%s, %s saved in %s" %(train_name, target_name, filepath)

def save_numpy_data(filepath, data, data_name='test'):
	io = IO()
	io.save_numpy_data(data, filepath, data_name)
	print "%s saved in %s" %(data_name, filepath)

# load numpy train & label data
def load_XY(filepath, X_name='X', Y_name='Y'):
	io = IO()
	X = io.get_numpy_data(filepath, X_name)
	Y  = io.get_numpy_data(filepath, Y_name)
	print "%s, %s loaded from %s" %(X_name, Y_name, filepath)
	return X, Y

def load_numpy_data(filepath, file_name):
	io = IO()
	data = io.get_numpy_data(filepath, file_name)
	print "%s loaded from %s" %(file_name, filepath)
	return data

def shuffle_train_data(train, target):
	io = IO()
	# get indices to shuffle
	shuffle_index = io.get_shuffle_index(len(target))
	#shuffle train data
	shuffled_train = io.shuffle_matrix_rows(train, shuffle_index)
	#shuffle train target
	shuffled_target = io.shuffle_array(target, shuffle_index)
	print "train & target shuffled"
	return shuffled_train, shuffled_target

def train_classifier(classifier, X, Y):
	start_time = time()
	classifier.train(X, Y)
	print "training completed in  %0.1f minutes" %((time() - start_time) / 60)
	return classifier

def get_random_forest(train, target, n_estimator=100):
	start_time = time()
	rf = Random_Forest(n_estimator) 
	rf.train(train, target)
	print "random forest, n_estimator= %d, trained, elapsed time: %0.1f" %( n_estimator, time() - start_time)
	return rf

def predict(classifier, test, target=None):
	io = IO()
	predict = classifier.predict(test)
	if target is None:
		# io.get_submission(predict)
		print "returning prediction"
		return predict
	else:
		score = io.get_score(predict, target)
		print "score is %0.4f, returning predict & score" %score
		return predict, score

def get_submission(filepath, file_name, Y, fmt='%d', header=None, delimiter=','):
	# X = first column, Y will be in second column
	X = [i+1 for i in range(len(Y))]
	fullfilename = filepath + '/' + file_name

	io = IO()
	io.get_submission(fullfilename, X, Y, fmt, header, delimiter)
	return

	