from sklearn.naive_bayes import GaussianNB

class Gaussian_NB(object):
	
	def __init__(self):
		self.classifier = GaussianNB()

	def train(self, train_data, train_label):
		self.classifier.fit(train_data, train_label)
		return

	def predict(self, test_data):
		return self.classifier.predict(test_data)